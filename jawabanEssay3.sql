CREATE DATABASE myshop;

CREATE TABLE customers (
    id int not null AUTO_INCREMENT PRIMARY KEY, 
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

CREATE TABLE orders (
    id int not null AUTO_INCREMENT PRIMARY KEY, 
    amount varchar(255),
    customer_id int not null,
    FOREIGN KEY fk_cus(customer_id) 
    REFERENCES customers(id)
);




<?php
     
    function hitung($string_data) {

        if(preg_match('/(\d+)(?:\s*)([\+\-\*\:\%])(?:\s*)(\d+)/', $string_data, $matches) !== FALSE){
            $operator = $matches[2];
            switch($operator){
                case '+':
                    $p = $matches[1] + $matches[3];
                    break;
                case '-':
                    $p = $matches[1] - $matches[3];
                    break;
                case '*':
                    $p = $matches[1] * $matches[3];
                    break;
                case ':':
                    $p = $matches[1] / $matches[3];
                    break;
                case '%':
                    $p = $matches[1] % $matches[3];
                    break;
            }
            return $p;
        }
    }

  echo hitung("102*2");
  echo "<br>";
  echo hitung("2+3");
  echo "<br>";
  echo hitung("100:25");
  echo "<br>";
  echo hitung("10%2");
  echo "<br>";
  echo hitung("99-2");



?>

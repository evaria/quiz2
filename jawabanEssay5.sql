SELECT customers.name AS customer_name, SUM(amount) AS total_amount
FROM customers
LEFT JOIN orders ON customers.id = orders.customer_id
GROUP BY customers.name;
